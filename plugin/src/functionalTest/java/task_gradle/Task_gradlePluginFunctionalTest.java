/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package task_gradle;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;
import java.nio.file.Files;

import org.gradle.testkit.runner.GradleRunner;
import org.gradle.testkit.runner.BuildResult;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * A simple functional test for the 'task_gradle.greeting' plugin.
 */
public class Task_gradlePluginFunctionalTest {
    @Test
    public void canRunTask() throws IOException {
        // Setup the test build
        File projectDir = new File("build/functionalTest");
        Files.createDirectories(projectDir.toPath());
        writeString(new File(projectDir, "settings.gradle"), "");
        writeString(new File(projectDir, "build.gradle"),
                "plugins {" +
                        "  id('task_gradle.convertJsonSchemaToPojo')" +
                        "}\n" +
                        "convertJsonSchemaToPojo {" +
                        "  schemasFolder = 'd:\\res\\files\\'\n" +
                        "  pojoFolder = 'd:\\res\\pojos\\'" +
                        "}");


        // Run the build
        GradleRunner runner = GradleRunner.create();
        runner.forwardOutput();
        runner.withPluginClasspath();
        runner.withArguments("convertJsonSchemaToPojo");
        runner.withProjectDir(projectDir);
        BuildResult result = runner.build();

        // Verify the result
        assertTrue(result.getOutput()
                .contains("Converted to Pojo files from json-schemas converted 5 files"));
    }

    private void writeString(File file, String string) throws IOException {
        try (Writer writer = new FileWriter(file)) {
            writer.write(string);
        }
    }
}
