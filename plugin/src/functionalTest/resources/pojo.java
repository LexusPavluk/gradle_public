/**
 * A representation of a person, company, organization, or place
*/

package src.test.resources.files;

import java.util.List;
import java.util.Objects;


public class ArrThings {
	 private  Object[] fruits;
	/**
 	* Do I like this vegetable?
	*/
	 private  Object[] vegetables;

	 public ArrThings(Object[] fruits, Object[] vegetables) {
		 this.fruits = fruits;
		 this.vegetables = vegetables;
	}

	public Object[] getFruits() {
		return fruits;
	}

	public Object[] getVegetables() {
		return vegetables;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ArrThings)) return false;
        ArrThings arrThings = (ArrThings) o;
        return Objects.equals(fruits, Object[].fruits) && Objects.equals(vegetables, Object[].vegetables);
	}

	@Override
    public int hashCode() {
        return Objects.hash(fruits, vegetables);
    }
 
}

package src.test.resources.files;

import java.util.List;
import java.util.Objects;


public class _2D3DDictionary {
	 private  Double x;
	 private  Double y;
	 private  Double z;
	 private  Double value;

	 public _2D3DDictionary(Double x, Double y, Double z, Double value) {
		 this.x = x;
		 this.y = y;
		 this.z = z;
		 this.value = value;
	}

	public Double getX() {
		return x;
	}

	public Double getY() {
		return y;
	}

	public Double getZ() {
		return z;
	}

	public Double getValue() {
		return value;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof _2D3DDictionary)) return false;
        _2D3DDictionary __2D3DDictionary = (_2D3DDictionary) o;
        return Objects.equals(x, Double.x) && Objects.equals(y, Double.y) && Objects.equals(z, Double.z) && Objects.equals(value, Double.value);
	}

	@Override
    public int hashCode() {
        return Objects.hash(x, y, z, value);
    }
 
}
/**
 * A geographical coordinate.
*/

package src.test.resources.files;

import java.util.List;
import java.util.Objects;


public class LongitudeAndLatitudeValues {
	 private  Double latitude;
	 private  Double longitude;

	 public LongitudeAndLatitudeValues(Double latitude, Double longitude) {
		 this.latitude = latitude;
		 this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LongitudeAndLatitudeValues)) return false;
        LongitudeAndLatitudeValues longitudeAndLatitudeValues = (LongitudeAndLatitudeValues) o;
        return Objects.equals(latitude, Double.latitude) && Objects.equals(longitude, Double.longitude);
	}

	@Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }
 
}

package src.test.resources.files;

import java.util.List;
import java.util.Objects;


public class Person {
	/**
 	* The person's first name.
	*/
	 private  String firstName;
	/**
 	* The person's last name.
	*/
	 private  String lastName;
	/**
 	* Age in years which must be equal to or greater than zero.
	*/
	 private  Integer age;

	 public Person(String firstName, String lastName, Integer age) {
		 this.firstName = firstName;
		 this.lastName = lastName;
		 this.age = age;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Integer getAge() {
		return age;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, String.firstName) && Objects.equals(lastName, String.lastName) && Objects.equals(age, Integer.age);
	}

	@Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, age);
    }
 
}
/**
 * Schema for the striped object specification file
*/

package src.test.resources.files;

import java.util.List;
import java.util.Objects;


public class Striped {
	 private  String code;
	 private  Double width;
	 private  Double stripe_length;

	 public Striped(String code, Double width, Double stripe_length) {
		 this.code = code;
		 this.width = width;
		 this.stripe_length = stripe_length;
	}

	public String getCode() {
		return code;
	}

	public Double getWidth() {
		return width;
	}

	public Double getStripe_length() {
		return stripe_length;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Striped)) return false;
        Striped striped = (Striped) o;
        return Objects.equals(code, String.code) && Objects.equals(width, Double.width) && Objects.equals(stripe_length, Double.stripe_length);
	}

	@Override
    public int hashCode() {
        return Objects.hash(code, width, stripe_length);
    }
 
}
