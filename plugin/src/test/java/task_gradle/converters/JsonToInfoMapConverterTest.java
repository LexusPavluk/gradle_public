/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import task_gradle.converters.factory.AbstractFactory;
import task_gradle.converters.factory.ValueFactory;
import task_gradle.dto.Field;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static task_gradle.converters.JsonSchemasText.*;
import static task_gradle.converters.SchemaConstants.SCHEMA_PROPERTIES;

class JsonToInfoMapConverterTest {
    private static AbstractFactory<Object> valueFactory;

    @BeforeAll
    static void initAll() {
        valueFactory = ValueFactory.getInstance();
    }

    @ParameterizedTest()
    @MethodSource ("schemasAndCountProvider")
    void exploderTest_validJson1ToMapPassed(String schema, int count) {
        JsonToInfoMapConverter mapper = new JsonToInfoMapConverter(valueFactory);
        Map<String, Object> schemaMap = mapper.convert(schema).get();
        List<?> fields = (List<?>) schemaMap.get(SCHEMA_PROPERTIES);
        assertEquals(count, fields.size());
        assertTrue(fields.get(0) instanceof Field);
    }

    static Stream<Arguments> schemasAndCountProvider() {
        return Stream.of(
                Arguments.arguments(VALID_SCHEMA_1, 2),
                Arguments.arguments(VALID_SCHEMA_2, 3),
                Arguments.arguments(VALID_SCHEMA_3, 4),
                Arguments.arguments(VALID_SCHEMA_4, 2)
        );
    }

}