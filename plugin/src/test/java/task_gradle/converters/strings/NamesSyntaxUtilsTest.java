/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters.strings;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static task_gradle.converters.strings.NamesSyntaxUtils.*;

class NamesSyntaxUtilsTest {

    @Test
    void classNameTest_RightSentenseToClassNameEqualsRightClassName() {
        String src = "ArrThingsSchema.json ";
        String expected = "ArrThings";
        String actual = className(src);
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("fileNamesProvider")
    void jsonFileNameWithoutExtAndSchemaTest_OK(String filename, String expectedName) {
        String actual = jsonFileNameWithoutExtAndSchema(filename);
        assertEquals(expectedName, actual);
    }

    static Stream<Arguments> fileNamesProvider() {
        return Stream.of(
                Arguments.arguments("2D3DDictionarySchema.json", "2D3DDictionary"),
                Arguments.arguments("ArrThings.json", "ArrThings"),
                Arguments.arguments("ArrThingsSchema.json", "ArrThings"),
                Arguments.arguments("LatLng.json", "LatLng"),
                Arguments.arguments("LatLngSchema.json", "LatLng"),
                Arguments.arguments("Person.json", "Person"),
                Arguments.arguments("PersonSchema.json", "Person"),
                Arguments.arguments("Schema — копия.ssss", " — копия"),
                Arguments.arguments("striped.json", "striped"),
                Arguments.arguments("stripedSchema.json", "striped")
        );
    }


    @ParameterizedTest
    @MethodSource("sentencesProvider")
    void stringToCamelCaseTest_SentenceToCamelCaseOk(String sentence, String expectedString) {
        String actual = stringToCamelCase(sentence);
        assertEquals(expectedString, actual);
    }

    static Stream<Arguments> sentencesProvider() {
        return Stream.of(
                Arguments.arguments("2 D 3 D Dictionary Schema.json", "2D3DDictionarySchemaJson"),
                Arguments.arguments("Arr Things.json", "ArrThingsJson"),
                Arguments.arguments("Arr ThingsSchema.json", "ArrThingsSchemaJson"),
                Arguments.arguments("Lat Lng.json", "LatLngJson"),
                Arguments.arguments("Lat LngSchema.json", "LatLngSchemaJson"),
                Arguments.arguments("Person . json", "PersonJson"),
                Arguments.arguments("Person Schema.json", "PersonSchemaJson"),
                Arguments.arguments("Schema — копия.ssss", "SchemaКопияSsss"),
                Arguments.arguments("striped.json", "StripedJson"),
                Arguments.arguments("striped Schema.json", "StripedSchemaJson"),
                Arguments.arguments("Longitude and Latitude Values", "LongitudeAndLatitudeValues"),
                Arguments.arguments("A geographical coordinate.", "AGeographicalCoordinate"),
                Arguments.arguments("A representation of a person, company, organization, or place",
                        "ARepresentationOfAPersonCompanyOrganizationOrPlace"),
                Arguments.arguments("Do I like this vegetable?", "DoILikeThisVegetable"),
                Arguments.arguments("Age in years which must be equal to or greater than zero.",
                        "AgeInYearsWhichMustBeEqualToOrGreaterThanZero")
        );
    }

}