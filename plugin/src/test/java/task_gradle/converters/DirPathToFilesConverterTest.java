package task_gradle.converters;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class DirPathToFilesConverterTest {
    static File tempDir;

    @BeforeAll
    static void initAll() {
        tempDir = FilesMock.madeFullContentDir();
    }

    @Test
    @Disabled
    void initAllTest_AllFilesIsCreated() {
        assertEquals(FilesMock.FILE_NAMES.length, Objects.requireNonNull(tempDir.list()).length);
    }

    @Test
    void convertTest_allJsonFiles() {
        String extension = ".json";
        String delimiter = ",\n";
        DirPathToFilesConverter converter =
                new DirPathToFilesConverter(extension);
        String expected = Arrays.stream(FilesMock.FILE_NAMES)
                .filter(string -> string.contains(extension))
                .sorted()
                .collect(Collectors.joining(",\n"));
        String actual = converter.convert(FilesMock.TEMP_DIR_PATH).orElseThrow()
                .stream()
                .map(File::getName)
                .sorted()
                .collect(Collectors.joining(delimiter));
        assertEquals(expected, actual);
    }

    @Test
    void convertTest_JsonSchemaFiles() {
        String extension = ".json";
        String regex = "[Ss]chema";
        String delimiter = ",\n";
        DirPathToFilesConverter converter =
                new DirPathToFilesConverter(extension, regex);
        Pattern schemaPattern = Pattern.compile(regex);
        String expected = Arrays.stream(FilesMock.FILE_NAMES)
                .filter(s -> s.endsWith(extension))
                .filter(s -> schemaPattern.matcher(s).find())
                .sorted()
                .collect(Collectors.joining(delimiter));
        Set<File> files = converter.convert(tempDir.getAbsolutePath()).orElseThrow();
        String actual = files.stream()
                .filter(Objects::nonNull)
                .map(File::getName)
                .sorted()
                .collect(Collectors.joining(delimiter));
        assertEquals(expected, actual);
    }

    @Test
    void convertTest_NoSuchFiles() {
        String extension = ".txt";
        String regex = "Tractor";
        String delimiter = ",\n";
        DirPathToFilesConverter converter =
                new DirPathToFilesConverter(extension, regex);
        Pattern schemaPattern = Pattern.compile(regex);
        String expected = Arrays.stream(FilesMock.FILE_NAMES)
                .filter(s -> s.endsWith(extension))
                .filter(s -> schemaPattern.matcher(s).find())
                .sorted()
                .collect(Collectors.joining(delimiter));
        Set<File> files = converter.convert(tempDir.getAbsolutePath()).orElseThrow();
        String actual = files.stream()
                .filter(Objects::nonNull)
                .map(File::getName)
                .sorted()
                .collect(Collectors.joining(delimiter));
        assertEquals(expected, actual);
    }

    @AfterAll
    static void tearDownAll() throws IOException {
        if(!FilesMock.deleteTempDirIfExist())
            throw new IOException("Temp Dir deleting error !!!");
    }

}