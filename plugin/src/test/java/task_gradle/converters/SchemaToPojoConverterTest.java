/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import task_gradle.dto.ElementWrapper;
import task_gradle.dto.PojoString;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertTrue;

class SchemaToPojoConverterTest {
    static final StringBuilder pojoText = new StringBuilder();
    static List<ElementWrapper> schemas;
    static final String dirPath = "src\\test\\resources\\files\\";
    static Converter<String, Set<File>> dirPathToFilesConverter;
    static Converter<File, ElementWrapper> fileJsonSrcConverter;
    final SchemaToPojoConverter converter = new SchemaToPojoConverter(dirPath);

    @BeforeAll
    static void initAll() {
        dirPathToFilesConverter = new DirPathToFilesConverter(".json", "[Ss]chema");
        fileJsonSrcConverter = new FileToJsonElemConverter();
        schemas = Objects.requireNonNull(dirPathToFilesConverter.convert(dirPath)
                .orElse(new HashSet<>()))
                .stream()
                .map(fileJsonSrcConverter::convert)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    @AfterAll
    static void tearDown() throws IOException {
        FileWriter writer = new FileWriter("src\\test\\resources\\pojo.java");
        String text = pojoText.toString();
        writer.write(text);
        writer.close();
    }


    @Test
    void convertTest_ValidElementWrapperConvertToPojoIsTrue() {
        schemas.stream()
                .map(converter::convert)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(PojoString::getPojoString)
//                .peek(System.out::println)
                .peek(pojoText::append)
                .forEach(pojo -> assertTrue(Objects.nonNull(pojo)));
    }

}