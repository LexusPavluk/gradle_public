/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters;

import org.junit.jupiter.api.Test;
import task_gradle.dto.ElementWrapper;
import task_gradle.dto.PojoString;

import java.io.File;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

class PojoToFileConverterTest {
    public static final String EXTENSION = ".json";
    public static final String FILE_SUFFIX_REGEX = "[Ss]chema";
    final String outputDirPath = "src\\test\\resources\\pojos\\";
    final String inputDirPath = "src\\test\\resources\\files\\";

    final Converter<String, Set<File>> pathToFiles =
            new DirPathToFilesConverter(EXTENSION, FILE_SUFFIX_REGEX);
    final Converter<File, ElementWrapper> toJsonConverter = new FileToJsonElemConverter();
    final Converter<ElementWrapper, PojoString> toPojoConverter =
            new SchemaToPojoConverter(outputDirPath);
    final Converter<PojoString, File> toFileConverter = new PojoToFileConverter(outputDirPath);


    @Test
    void convert() {
        Set<File> files = pathToFiles.convert(inputDirPath).orElseThrow();
        files.stream()
                .map(toJsonConverter::convert)
                .map(Optional::orElseThrow)
                .map(toPojoConverter::convert)
                .map(Optional::orElseThrow)
                .map(toFileConverter::convert)
                .map(Optional::orElseThrow)
                .forEach(file -> assertTrue(file.exists()));
    }
}