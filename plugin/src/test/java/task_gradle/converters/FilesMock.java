/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

class FilesMock {
    static final Logger LOGGER = Logger.getLogger(FilesMock.class);
    static final String TEMP_DIR_PATH = "src\\test\\resources\\temp";
    static final String[] FILE_NAMES = new String[]{"ArrThings.json", "ArrThingsSchema.json", "2D3DDictionarySchema.json",
            "LatLng.json", "LatLngSchema.sss", "Person.json", "PersonSchema.json", "PersonSchema.js",
            "striped.json"};
    static final String JSON_EXT = ".json";
    static final String JSON_STRING = "{\n" +
                    "  \"firstName\": \"John\",\n" +
                    "  \"lastName\": \"Doe\",\n" +
                    "  \"age\": 21\n" +
                    "}";
    static File tempDir;

    static File madeFullContentDir() {
        tempDir = new File(TEMP_DIR_PATH);
        if (deleteTempDirIfExist() && (tempDir = new File(TEMP_DIR_PATH)).mkdir()) {
            Arrays.stream(FILE_NAMES)
                    .map(s -> new File(TEMP_DIR_PATH, File.separator + s))
                    .filter(FilesMock::createFileOnDisk)
                    .filter(file -> file.getName().contains(JSON_EXT))
                    .forEach(FilesMock::writeInFile);
        }
        return tempDir;
    }

    private static boolean createFileOnDisk(File file) {
        try {
            return file.createNewFile();
        } catch (IOException e) {
            LOGGER.info(file.getAbsolutePath());
            return false;
        }
    }

    private static void writeInFile(File file) {
        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(FilesMock.JSON_STRING);
        } catch (IOException e) {
            LOGGER.info(file.getAbsolutePath());
        }
    }

    static boolean deleteTempDirIfExist() {
        if (tempDir.exists()) {
            Arrays.stream(Objects.requireNonNull(tempDir.listFiles()))
                    .filter(File::delete)
                    .forEach(file -> LOGGER.info(file.getName() +
                            " is " + (file.exists() ? "not " : "") + "deleted..."));
            boolean deletedDir = tempDir.delete();
            String blabla = "Temp dir is not deleted";
            System.out.println(!deletedDir ? blabla : "");
        }
        return !tempDir.exists();
    }


}
