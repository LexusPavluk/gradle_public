/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters;

public class JsonSchemasText {
    public static final String VALID_SCHEMA_1 =
            "{\n" +
                    "  \"$id\": \"https://example.com/geographical-location.schema.json\",\n" +
                    "  \"$schema\": \"http://json-schema.org/draft-07/schema#\",\n" +
                    "  \"title\": \"Longitude and Latitude Values\",\n" +
                    "  \"description\": \"A geographical coordinate.\",\n" +
                    "  \"required\": [ \"latitude\", \"longitude\" ],\n" +
                    "  \"type\": \"object\",\n" +
                    "  \"properties\": {\n" +
                    "    \"latitude\": {\n" +
                    "      \"type\": \"number\",\n" +
                    "      \"minimum\": -90,\n" +
                    "      \"maximum\": 90\n" +
                    "    },\n" +
                    "    \"longitude\": {\n" +
                    "      \"type\": \"number\",\n" +
                    "      \"minimum\": -180,\n" +
                    "      \"maximum\": 180\n" +
                    "    }\n" +
                    "  }\n" +
                    "}";

    public static final String VALID_SCHEMA_2 =
            "{\n" +
                    "  \"$id\": \"https://example.com/person.schema.json\",\n" +
                    "  \"$schema\": \"http://json-schema.org/draft-07/schema#\",\n" +
                    "  \"title\": \"Person\",\n" +
                    "  \"type\": \"object\",\n" +
                    "  \"properties\": {\n" +
                    "    \"firstName\": {\n" +
                    "      \"type\": \"string\",\n" +
                    "      \"description\": \"The person's first name.\"\n" +
                    "    },\n" +
                    "    \"lastName\": {\n" +
                    "      \"type\": \"string\",\n" +
                    "      \"description\": \"The person's last name.\"\n" +
                    "    },\n" +
                    "    \"age\": {\n" +
                    "      \"description\": \"Age in years which must be equal to or greater than zero.\",\n" +
                    "      \"type\": \"integer\",\n" +
                    "      \"minimum\": 0\n" +
                    "    }\n" +
                    "  }\n" +
                    "}";

    public static final String VALID_SCHEMA_3 = "{\n" +
            "  \"type\": \"object\",\n" +
            "  \"properties\": {\n" +
            "    \"value\": {\n" +
            "      \"type\": \"number\",\n" +
            "      \"minimum\": 0\n" +
            "    },\n" +
            "    \"x\": {\n" +
            "      \"type\": \"number\",\n" +
            "      \"minimum\": -1,\n" +
            "      \"maximum\": 1\n" +
            "    },\n" +
            "    \"y\": {\n" +
            "      \"type\": \"number\",\n" +
            "      \"minimum\": -1,\n" +
            "      \"maximum\": 1\n" +
            "    },\n" +
            "    \"z\": {\n" +
            "      \"type\": \"number\",\n" +
            "      \"minimum\": -1,\n" +
            "      \"maximum\": 1\n" +
            "    }\n" +
            "  },\n" +
            "  \"required\": [\n" +
            "    \"value\",\n" +
            "    \"x\",\n" +
            "    \"y\"\n" +
            "  ]\n" +
            "}";

    public static final String VALID_SCHEMA_4 =
            "{\n" +
                    "  \"$id\": \"https://example.com/arrays.schema.json\",\n" +
                    "  \"$schema\": \"http://json-schema.org/draft-07/schema#\",\n" +
                    "  \"description\": \"A representation of a person, company, organization, or place\",\n" +
                    "  \"type\": \"object\",\n" +
                    "  \"properties\": {\n" +
                    "    \"fruits\": {\n" +
                    "      \"type\": \"array\",\n" +
                    "      \"items\": {\n" +
                    "        \"type\": \"string\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    \"vegetables\": {\n" +
                    "      \"type\": \"array\",\n" +
                    "      \"description\": \"Do I like this vegetable?\",\n" +
                    "      \"items\": {\n" +
                    "        \"type\": \"boolean\"\n" +
                    "      }\n" +
                    "    }\n" +
                    "  }\n" +
                    "}";
}
