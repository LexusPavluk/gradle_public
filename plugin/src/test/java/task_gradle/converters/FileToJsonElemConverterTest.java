/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import task_gradle.dto.ElementWrapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class FileToJsonElemConverterTest {
    final static Logger LOGGER = Logger.getLogger(FileToJsonElemConverterTest.class);
    static List<File> files;
    static Converter<File, ElementWrapper> fileToJsonElemConverter;

    @BeforeAll
    static void initAll(){
        fileToJsonElemConverter = new FileToJsonElemConverter();
        files = Arrays.stream(Objects.requireNonNull(FilesMock.madeFullContentDir().listFiles()))
                .sorted()
                .collect(Collectors.toList());
    }

//    @Test
//    @Disabled
//    void initAllTest_AllFilesIsCreated() {
//        assertEquals(FilesMock.FILE_NAMES.length,
//                Objects.requireNonNull(FilesMock.tempDir.list()).length);
//    }

    @Test
    void convertTest_allFilesConvertedToJsonSrc() {
        long expected = files.size();
        long actual = files.stream()
                .map(file ->
                        new Object[]{
                                file, fileToJsonElemConverter.convert(file).orElseThrow()})
                .filter(this::filenameIsEqualsJsonSrcName)
                .filter(this::fileContentIsEqualsJsonContent)
                .count();
        assertEquals(expected, actual);
    }

    private boolean filenameIsEqualsJsonSrcName(Object[] arr) {
        String filename = ((File) arr[0]).getName();
        String jsonName = ((ElementWrapper) arr[1]).getName();
        return filename.equals(jsonName);
    }

    private boolean fileContentIsEqualsJsonContent(Object[] arr) {
        try {
            String fileContent = Files.lines(((File) arr[0]).toPath())
                    .collect(Collectors.joining());
            String jsonContent = (String) ((ElementWrapper) arr[1]).getValue();
            return fileContent.equals(jsonContent);
        } catch (IOException e) {
            LOGGER.warn(e);
            return false;
        }
    }

    @AfterAll
    static void tearDownAll() throws IOException {
        if(!FilesMock.deleteTempDirIfExist())
            throw new IOException("Temp Dir deleting error !!!");
    }

}