/**
 * A representation of a person, company, organization, or place
*/

package src.test.resources.pojos;

import java.util.List;
import java.util.Objects;


public class ArrThings {
	 private  Object[] fruits;
	/**
 	* Do I like this vegetable?
	*/
	 private  Object[] vegetables;

	 public ArrThings(Object[] fruits, Object[] vegetables) {
		 this.fruits = fruits;
		 this.vegetables = vegetables;
	}

	public Object[] getFruits() {
		return fruits;
	}

	public Object[] getVegetables() {
		return vegetables;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ArrThings)) return false;
        ArrThings arrThings = (ArrThings) o;
        return Objects.equals(fruits, Object[].fruits) && Objects.equals(vegetables, Object[].vegetables);
	}

	@Override
    public int hashCode() {
        return Objects.hash(fruits, vegetables);
    }
 
}
