/**
 * Schema for the striped object specification file
*/

package src.test.resources.pojos;

import java.util.List;
import java.util.Objects;


public class Striped {
	 private  String code;
	 private  Double width;
	 private  Double stripe_length;

	 public Striped(String code, Double width, Double stripe_length) {
		 this.code = code;
		 this.width = width;
		 this.stripe_length = stripe_length;
	}

	public String getCode() {
		return code;
	}

	public Double getWidth() {
		return width;
	}

	public Double getStripe_length() {
		return stripe_length;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Striped)) return false;
        Striped striped = (Striped) o;
        return Objects.equals(code, String.code) && Objects.equals(width, Double.width) && Objects.equals(stripe_length, Double.stripe_length);
	}

	@Override
    public int hashCode() {
        return Objects.hash(code, width, stripe_length);
    }
 
}
