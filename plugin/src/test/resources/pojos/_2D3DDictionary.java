
package src.test.resources.pojos;

import java.util.List;
import java.util.Objects;


public class _2D3DDictionary {
	 private  Double x;
	 private  Double y;
	 private  Double z;
	 private  Double value;

	 public _2D3DDictionary(Double x, Double y, Double z, Double value) {
		 this.x = x;
		 this.y = y;
		 this.z = z;
		 this.value = value;
	}

	public Double getX() {
		return x;
	}

	public Double getY() {
		return y;
	}

	public Double getZ() {
		return z;
	}

	public Double getValue() {
		return value;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof _2D3DDictionary)) return false;
        _2D3DDictionary __2D3DDictionary = (_2D3DDictionary) o;
        return Objects.equals(x, Double.x) && Objects.equals(y, Double.y) && Objects.equals(z, Double.z) && Objects.equals(value, Double.value);
	}

	@Override
    public int hashCode() {
        return Objects.hash(x, y, z, value);
    }
 
}
