/**
 * A geographical coordinate.
*/

package src.test.resources.pojos;

import java.util.List;
import java.util.Objects;


public class LongitudeAndLatitudeValues {
	 private  Double latitude;
	 private  Double longitude;

	 public LongitudeAndLatitudeValues(Double latitude, Double longitude) {
		 this.latitude = latitude;
		 this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LongitudeAndLatitudeValues)) return false;
        LongitudeAndLatitudeValues longitudeAndLatitudeValues = (LongitudeAndLatitudeValues) o;
        return Objects.equals(latitude, Double.latitude) && Objects.equals(longitude, Double.longitude);
	}

	@Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }
 
}
