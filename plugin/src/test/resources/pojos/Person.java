
package src.test.resources.pojos;

import java.util.List;
import java.util.Objects;


public class Person {
	/**
 	* The person's first name.
	*/
	 private  String firstName;
	/**
 	* The person's last name.
	*/
	 private  String lastName;
	/**
 	* Age in years which must be equal to or greater than zero.
	*/
	 private  Integer age;

	 public Person(String firstName, String lastName, Integer age) {
		 this.firstName = firstName;
		 this.lastName = lastName;
		 this.age = age;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Integer getAge() {
		return age;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, String.firstName) && Objects.equals(lastName, String.lastName) && Objects.equals(age, Integer.age);
	}

	@Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, age);
    }
 
}
