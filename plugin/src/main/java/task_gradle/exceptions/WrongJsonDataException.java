/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.exceptions;

public class WrongJsonDataException extends IllegalArgumentException {
    /**
     * Constructs a new exception with the specified detail message and
     * cause.
     *
     * @param message the detail message (which is saved for later retrieval
     *                by the {@link Throwable#getMessage()} method).
     * @param cause   the cause (which is saved for later retrieval by the
     *                {@link Throwable#getCause()} method).  (A {@code null} value
     *                is permitted, and indicates that the cause is nonexistent or
     *                unknown.)
     */
    public WrongJsonDataException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs an <code>IllegalArgumentException</code> with the
     * specified detail message.
     *
     * @param s the detail message.
     */
    public WrongJsonDataException(String s) {
        super(s);
    }

    @Override
    public String toString() {
        return "WrongJsonDataException{" +
                "wrong date='" + super.getMessage() + '\'' +
                '}';
    }
}
