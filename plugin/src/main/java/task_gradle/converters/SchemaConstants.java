package task_gradle.converters;

public class SchemaConstants {
    public static final String SCHEMA_$ID = "$id";
    public static final String SCHEMA_$SCHEMA = "$schema";
    public static final String SCHEMA_TITLE = "title";
    public static final String SCHEMA_DESCRIPTION = "description";
    public static final String SCHEMA_TYPE = "type";
    public static final String SCHEMA_PROPERTIES = "properties";

    public static final String SCHEMA_MAXIMUM = "maximum";
    public static final String SCHEMA_MINIMUM = "minimum";
    public static final String SCHEMA_REQUIRED = "required";

    public static final String FIELD_TYPE_OBJECT = "object";
    public static final String FIELD_TYPE_ARRAY = "array";
    public static final String FIELD_TYPE_STRING = "string";
    public static final String FIELD_TYPE_BOOLEAN = "boolean";
    public static final String FIELD_TYPE_INTEGER = "integer";
    public static final String FIELD_TYPE_NUMBER = "number";

    public static final String FIELD_ARRAY_ITEMS = "items";
    public static final String FIELD = "field";
    public static final String PACKAGE = "package";

}
