package task_gradle.converters;

import task_gradle.converters.factory.ValueFactory;
import task_gradle.converters.strings.PojoTextBuilder;
import task_gradle.dto.ElementWrapper;
import task_gradle.dto.PojoString;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static task_gradle.converters.SchemaConstants.*;
import static task_gradle.converters.strings.NamesSyntaxUtils.className;

public class SchemaToPojoConverter implements Converter<ElementWrapper, PojoString> {

    /**
     * Путь выходной папки для сохранения готовых POJO-объектов.
     */
    private final String pakage;

    public SchemaToPojoConverter(String outputPath) {
        this.pakage = getPackage(outputPath);
    }

    /**
     * Конвертирует ElementWrapper, содержащий информацию об имени файла-jsonSchema
     * и его содержимом.
     *
     * @param jsonSchemaText - ElementWrapper, содержащий информацию
     *                       об имени файла-jsonSchema и его содержимом
     *                       в String объектах.
     * @return тип PojoString, содержащий информацию об имени
     * pojo-класса и его содержимого в String виде.
     */
    @Override
    public Optional<PojoString> convert(final ElementWrapper jsonSchemaText) {
        String schemaContent = (String) jsonSchemaText.getValue();
        Map<String, Object> pojoInfo = new JsonToInfoMapConverter(ValueFactory.getInstance())
                .convert(schemaContent).orElseThrow();

        if (pojoInfo.containsKey(SCHEMA_TITLE)) {
            pojoInfo.replace(
                    SCHEMA_TITLE,
                    className((String) pojoInfo.get(SCHEMA_TITLE)));
        } else {
            pojoInfo.put(
                    SCHEMA_TITLE,
                    className(jsonSchemaText.getName()));
        }

        pojoInfo.put(PACKAGE, pakage);

        PojoTextBuilder pojoBuilder = new PojoTextBuilder(pojoInfo);
        return Optional.of(
                new PojoString(
                        (String) pojoInfo.get(SCHEMA_TITLE),
                        pojoBuilder.getClassContentText()));
    }

    /**
     * @param outputPath - строка пути сохранения файлов POJO
     * @return - строка для определения {@code package} POJO - файла
     */
    private String getPackage(String outputPath) {
        return Arrays.stream(outputPath.split("\\\\"))
                .filter(s -> !s.contains(":"))
                .collect(Collectors.joining("."));
    }

}
