/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters.factory;

import task_gradle.converters.factory.value.PropertiesCreator;
import task_gradle.dto.ElementWrapper;

import java.util.Map;
import java.util.Optional;

import static task_gradle.converters.SchemaConstants.*;

/**
 * Фабрика, предоставляющая значение типа соответствующего ключу (согласно json schema)
 */
public class ValueFactory extends AbstractFactory<Object> {
    private static AbstractFactory<Object> INSTANCE;
    private static final String OTHER = "other";

    private ValueFactory() {
    }

    public static AbstractFactory<Object> getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ValueFactory();
        }
        return INSTANCE;
    }

    {
        keyHandler.put(SCHEMA_TITLE, String::valueOf);
        keyHandler.put(SCHEMA_DESCRIPTION, String::valueOf);
        keyHandler.put(SCHEMA_PROPERTIES, this::valueOfPropertyKey);
        keyHandler.put(SCHEMA_TYPE, this::valueOfTypeKey);
        keyHandler.put(SCHEMA_$ID, String::valueOf);
        keyHandler.put(SCHEMA_$SCHEMA, String::valueOf);
        keyHandler.put(OTHER, value -> value);
    }

    @Override
    public Optional<Object> getFrom(ElementWrapper source) {
        String key = source.getName();
        return isPresentKey(key) ?
                Optional.of(keyHandler.get(key).apply(source.getValue())) :
                Optional.of(keyHandler.get(OTHER).apply(source.getValue()));
    }

    private Object valueOfPropertyKey(Object propertiesSource) {
        return Optional.ofNullable(propertiesSource)
                .map(ObjectToMapCaster::castToMap)
                .map(fields -> new PropertiesCreator().create(fields).orElseThrow())
                .orElseThrow();
    }

    private Object valueOfTypeKey(Object typeSource) {
        return FieldsTypeFactory.getInstance()
                .getFrom(new ElementWrapper((String) typeSource, null)).orElse("Object");
    }

}
