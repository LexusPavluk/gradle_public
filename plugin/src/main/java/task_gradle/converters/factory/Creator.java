/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters.factory;

import java.util.Map;
import java.util.Optional;

public interface Creator<V> {

    Optional<V> create(Map<String, Object> source);
}
