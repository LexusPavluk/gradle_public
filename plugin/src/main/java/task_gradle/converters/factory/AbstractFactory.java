/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters.factory;

import task_gradle.dto.ElementWrapper;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

public abstract class AbstractFactory<T> {
    protected final Map<String, Function<Object, T>> keyHandler = new HashMap<>();

    public abstract Optional<T> getFrom(ElementWrapper source);

    protected boolean isPresentKey(String key) {
        return Objects.nonNull(keyHandler.get(key));
    }

}
