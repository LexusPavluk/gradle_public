/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters.factory;

import java.util.Map;
import java.util.Optional;

public class ObjectToMapCaster {

    /**
     * @param maybeMap - объект, проверяемый на принадлежность к интерфейсу Map типизированного
     *                 <String, Object>
     * @return true если объект - типа Map<String, Object>
     */
    public static Map<String, Object> castToMap(Object maybeMap) {
        return Optional.ofNullable(maybeMap)
                .filter(mayMap -> mayMap instanceof Map)
                .map(mayMap -> (Map<?, ?>) mayMap)
                .filter(someMap -> someMap.keySet().stream()
                        .findFirst().orElse(null) instanceof String)
                .map(o -> (Map<String, Object>) o)
                .orElseThrow(ClassCastException::new);
    }

}
