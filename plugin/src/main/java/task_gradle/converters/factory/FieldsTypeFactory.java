/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters.factory;

import task_gradle.converters.factory.type.ArrayTypeCreator;
import task_gradle.dto.ElementWrapper;

import java.util.Map;
import java.util.Optional;

import static task_gradle.converters.SchemaConstants.*;

public class FieldsTypeFactory extends AbstractFactory<String> {
    private static final String OBJ_ARRAY = "Object[]";
    private static AbstractFactory<String> INSTANCE;

    private FieldsTypeFactory() {
    }

    public static AbstractFactory<String> getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FieldsTypeFactory();
        }
        return INSTANCE;
    }

    {
        keyHandler.put(FIELD_TYPE_OBJECT, value -> "Object");
        keyHandler.put(FIELD_TYPE_ARRAY, this::arrayType);
        keyHandler.put(FIELD_TYPE_STRING, value -> "String");
        keyHandler.put(FIELD_TYPE_BOOLEAN, value -> "Boolean");
        keyHandler.put(FIELD_TYPE_INTEGER, value -> "Integer");
        keyHandler.put(FIELD_TYPE_NUMBER, value -> "Double");
    }

    /**
     * @param source - пара "тип" - значение для массивов: "Map<String, Object> по ключу "items".
     * @return строковое представление типа объекта
     */
    @Override
    public Optional<String> getFrom(ElementWrapper source) {
        String key = source.getName().toLowerCase();
        return isPresentKey(key) ?
                Optional.of(keyHandler.get(key).apply(source.getValue())) :
                Optional.empty();
    }

    private String arrayType(Object value) {
        var some = value;
        return Optional.ofNullable(value)
                .map(ObjectToMapCaster::castToMap)
                .map(items -> items.get(FIELD_ARRAY_ITEMS))
                .map(ObjectToMapCaster::castToMap)
                .map(type -> new ArrayTypeCreator().create(type).orElse(OBJ_ARRAY))
                .orElse(OBJ_ARRAY);
    }

}
