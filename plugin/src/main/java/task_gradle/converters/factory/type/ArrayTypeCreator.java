/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters.factory.type;

import task_gradle.converters.SchemaConstants;
import task_gradle.converters.factory.Creator;
import task_gradle.converters.factory.FieldsTypeFactory;
import task_gradle.dto.ElementWrapper;

import java.util.Map;
import java.util.Optional;

public class ArrayTypeCreator implements Creator<String> {

    /**
     * Возвращает тип массива объектов в строковом виде.
     *
     * @param arraySource - пара "array" = соотвествующий ему из schema {текст},
     * @return Optional<String> - строки вида "ТипOбъекта[]".
     */
    @Override
    public Optional<String> create(Map<String, Object> arraySource) {
        return arraySource.entrySet().stream()
                .filter(entry -> entry.getKey().equals(SchemaConstants.SCHEMA_TYPE))
                .map(entry -> new ElementWrapper(entry.getValue().toString(), null))
                .map(elementWrapper -> FieldsTypeFactory.getInstance().
                        getFrom(elementWrapper) + "[]")
                .findFirst();
    }

}
