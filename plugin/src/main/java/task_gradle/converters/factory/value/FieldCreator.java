/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters.factory.value;

import task_gradle.converters.factory.Creator;
import task_gradle.converters.factory.ObjectToMapCaster;
import task_gradle.converters.strings.NamesSyntaxUtils;
import task_gradle.dto.Field;

import java.util.Map;
import java.util.Optional;

import static task_gradle.converters.SchemaConstants.SCHEMA_DESCRIPTION;
import static task_gradle.converters.SchemaConstants.SCHEMA_TYPE;

public class FieldCreator implements Creator<Field> {

    /**
     * Возвращает объект типа Field из строки.
     *
     * @param fieldSource - пара "имя поля" = соотвествующая ему строка json поля
     * @return Optional<Field> -  объект, содержащий полные сведения о поле класса.
     */
    @Override
    public Optional<Field> create(Map<String, Object> fieldSource) {
        Optional<String> name =
                fieldSource.keySet().stream().findFirst();
        Optional<Map<String, Object>> fieldData =
                name.map(fieldSource::get)
                        .map(ObjectToMapCaster::castToMap);
        Optional<String> type = fieldData
                .map(dataMap -> (String) dataMap.get(SCHEMA_TYPE));
        String description = fieldData
                .map(dataMap -> (String) dataMap.get(SCHEMA_DESCRIPTION)).orElse(null);

        return type.map(type1 -> new Field(
                type1,
                NamesSyntaxUtils.fieldName(name.get()),
                false,
                description));
    }

}
