/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters.factory.value;

import task_gradle.converters.factory.Creator;
import task_gradle.dto.Field;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class PropertiesCreator implements Creator<List<Field>> {

    /**
     * Возвращает тип объекта в строковом виде.
     *
     * @param fieldsMap - пара properties = соотвествующий ему из schema Map, содержащий пары:
     *                  имя поля : его свойства в виде Map
     * @return Optional<List < Field>> - список объектов Field
     * (содержащие информацию о формировании полей в POJO).
     */
    @Override
    public Optional<List<Field>> create(Map<String, Object> fieldsMap) {
        Creator<Field> creator = new FieldCreator();
        return Optional.of(fieldsMap.entrySet().stream()
                .map(Map::ofEntries)
                .map(creator::create)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList())
        );
    }
}
