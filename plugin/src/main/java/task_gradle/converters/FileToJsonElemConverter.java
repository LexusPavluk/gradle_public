package task_gradle.converters;

import task_gradle.dto.ElementWrapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileToJsonElemConverter extends AbstractConverter<File, ElementWrapper>{

    /**
     * Преобразует File в dto ElementWrapper.
     * @param source - файл источник данных.
     * @return - Optional ElementWrapper, с парой: имя файла - содержимое.
     */
    @Override
    public Optional<ElementWrapper> convert(File source) {
        return contentStrFrom(source)
                .map(s -> new ElementWrapper(source.getName(), s));
    }

    /**
     * Получает содержимое файла в виде строки.
     * @param file - источник содержимого.
     * @return - String, представляющий содержимое файла.
     */
    private Optional<String> contentStrFrom(File file) {
        try (Stream<String> lines = Files.lines(file.toPath())){
            return Optional.of(lines.collect(Collectors.joining()));
        } catch (IOException e) {
            LOGGER.warn("Error in-out of file: \n",e);
            return Optional.empty();
        }
    }
}
