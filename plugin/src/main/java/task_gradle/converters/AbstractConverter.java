/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters;

import org.apache.log4j.Logger;

/**
 * Абстрактный класс, имплементирующий {@code Converter<S,R>} добавляет Logger к
 * расширяющим его классам.
 *
 * @param <S> - входящий тип параметра конвертера.
 * @param <R> - результирующий тип параметра конвертера.
 */
public abstract class AbstractConverter<S,R> implements Converter<S,R>{
    protected final Logger LOGGER = Logger.getLogger(Converter.class);

}
