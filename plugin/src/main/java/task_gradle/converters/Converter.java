/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */
package task_gradle.converters;

import java.util.Optional;

/**
 * Интерфейс конвертера, принимающий источник типа {@code <S>},
 * преборазующий в результат типа {@code <R>}.
 *
 * @param <S> - входящий тип параметра конвертера.
 * @param <R> - результирующий тип параметра конвертера.
 */
public interface Converter<S, R> {

    Optional<R> convert(S source);
}
