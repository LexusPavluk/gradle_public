/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters;

import java.io.File;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Класс расширяющий AbstractConverter для преобразования пути папки с содрежащимися в ней
 * файлами в набор файлов
 */
public class DirPathToFilesConverter extends AbstractConverter<String, Set<File>> {
    /**
     * требуемое расширение фалов в формате ".{расширение}"
     */
    protected final String ext;
    /**
     * Регулярное выражения фильтрации имён требуемых файлов.
     */
    protected final Pattern namePattern;

    /**
     * Конструктор с параметрами "расширения" и регулярки под имя файла.
     * @param ext - расширение файла
     * @param regex - регулярное выражение для фильтрации по имени файла.
     */
    public DirPathToFilesConverter(String ext, String regex) {
        this.ext = ext;
        this.namePattern = Pattern.compile(regex);
    }
    /**
     * Конструктор с параметром "расширения" файла.
     * @param ext - расширение файла
     */
    public DirPathToFilesConverter(String ext) {
        this.ext = ext;
        namePattern = Pattern.compile("\\w");
    }

    /**
     * Преобразование пути директории в список файлов, содержащихся в ней, имена которых
     * удовлетворяют полям данного класса.
     * @param sourceDir - исходная папка - источник файлов.
     * @return - список файлов, удовлетворяющих условиям фильтрации согласно параметрам класса.
     */
    @Override
    public Optional<Set<File>> convert(String sourceDir) {
        File dir = new File(sourceDir);
        if (dir.exists()) {
            return Optional.of(
                    Arrays.stream(Objects.requireNonNull(
                            dir.listFiles((File d, String name) ->
                                    name.toLowerCase().endsWith(ext) &&
                                    namePattern.matcher(name).find())))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet()));
        }
        LOGGER.warn("Path: " + sourceDir + " is not exist.");
        return Optional.empty();
    }

}
