package task_gradle.converters;

import task_gradle.dto.PojoString;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class PojoToFileConverter  extends AbstractConverter<PojoString, File> {
    private final String dirPath;

    public PojoToFileConverter(String dirPath) {
        this.dirPath = dirPath;
    }

    @Override
    public Optional<File> convert(PojoString source) {
        String filePath = dirPath + source.getPojoName() + ".java";
        File pojo = new File(filePath);
        try(FileWriter writer = new FileWriter(pojo)) {
            writer.write(source.getPojoString());
        } catch (IOException e) {
            LOGGER.warn(e);
            return Optional.empty();
        }
        Path path = Path.of(filePath);
        String fileContent = null;
        try {
            fileContent = Files.lines(path)
                    .collect(Collectors.joining("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean isEquals = Objects.nonNull(fileContent) &&
                Objects.equals(
                fileContent.replaceAll("\\s", ""),
                source.getPojoString().replaceAll("\\s", ""));
        return isEquals ? Optional.of(pojo) : Optional.empty();

    }
}
