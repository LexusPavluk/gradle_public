/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters;

import task_gradle.converters.factory.AbstractFactory;
import task_gradle.dto.ElementWrapper;

import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static task_gradle.converters.strings.StringUtils.EMPTY_STRING;

public class JsonToInfoMapConverter extends AbstractConverter<String, Map<String, Object>> {
    private static final String KEY_REGEX = "(\"[^\"\\\\]*\":)";                // (\"[^\"\\]*\":)
    private static final Pattern KEY_PATTERN = Pattern.compile(KEY_REGEX);
    private static final String VALUE_REGEX = "(\"[^\"\\\\]*?\")|([-\\w]+)";         //   (\"[^\"\\]*?\")|([-\w]+)
    private static final Pattern VALUE_PATTERN = Pattern.compile(VALUE_REGEX);
    private static final String SIGN_REGEX = "[{},\\[\\]]{1}";                     //    [{},\\[\\]]{1}
    private static final Pattern BIT_PATTERN =
            Pattern.compile(SIGN_REGEX + "|" + KEY_REGEX + "|" + VALUE_REGEX);
    private static final String END_MATCHER = "end";

    private final Matcher bitMatcher = BIT_PATTERN.matcher(EMPTY_STRING);
    private final AbstractFactory<Object> valueFactory;
    private final ParsingStrategy strategy = new ParsingStrategy();
    private Map<String, Object> map;
    private List<Object> arr;
    private String key;
    private Object value;
    int endMatcherPos;
    boolean resume = true;

    public JsonToInfoMapConverter(AbstractFactory<Object> valueFactory) {
        this.valueFactory = valueFactory;
    }

    @Override
    public Optional<Map<String, Object>> convert(String source) {
        return Optional.of(schemaToInfoMap(source));
    }

    /**
     * Помещает ключ-значение в Map и обнуляет ключ-значение.
     */
    private void putAndNullifyKey_Value() {
        if (Objects.nonNull(value)) {
            valueFactory.getFrom(new ElementWrapper(key, value))
                    .ifPresent(v -> map.put(key, v));
            key = null;
            value = null;
        }
    }

    /**
     * @param quoted строка в кавычках (м.б. доп. знаки)
     * @return строку - содержимое кавычек
     */
    private String unQuoteString(String quoted) {
        int start = quoted.indexOf('"');
        int end = quoted.lastIndexOf('"');
        return quoted.substring(start + 1, end > 0 ? end : quoted.length());
    }

    private Map<String, Object> schemaToInfoMap(String json) {
        map = new HashMap<>();
        bitMatcher.reset(json);
        while (resume & bitMatcher.find()) {
            endMatcherPos = bitMatcher.end();
            String bit = bitMatcher.group();
            if (KEY_PATTERN.matcher(bit).matches())
                key = unQuoteString(bit);
            else if (VALUE_PATTERN.matcher(bit).matches())
                value = unQuoteString(bit);
            else strategy.getFrom(bit, json);
        }
        return map;
    }

    private class ParsingStrategy {
        protected final Map<String, Consumer<Object>> keyHandler = new HashMap<>();

        {
            keyHandler.put("{", this::startObject);
            keyHandler.put("}", value -> endObject());
            keyHandler.put(",", value -> nextElement());
            keyHandler.put("[", value -> startArray());
            keyHandler.put("]", value -> endArray());
        }

        public void getFrom(String keyStr, Object value) {
            keyHandler.get(keyStr).accept(value);
        }

        private void startObject(Object jObj) {
            String json = (String) jObj;
            Map<String, Object> innerMap = new JsonToInfoMapConverter(valueFactory).schemaToInfoMap(json.substring(endMatcherPos));
            int end = (Integer) innerMap.remove(END_MATCHER);
            bitMatcher.region(endMatcherPos + end, json.length());
            if (key == null)
                map = innerMap;
            else {
                value = innerMap;
                putAndNullifyKey_Value();
            }
        }

        private void endObject() {
            putAndNullifyKey_Value();
            map.put(END_MATCHER, endMatcherPos);
            resume = false;
        }

        private void nextElement() {
            if (arr == null)
                putAndNullifyKey_Value();
            else {
                arr.add(value);
                value = null;
            }
        }

        private void startArray() {
            arr = new ArrayList<>();
        }

        private void endArray() {
            arr.add(value);
            value = arr;
            arr = null;
        }

    }

}
