/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters.strings;

import task_gradle.dto.Field;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static task_gradle.converters.SchemaConstants.*;
import static task_gradle.converters.strings.StringUtils.*;

/**
 * Класс, формирующий текст POJO-класса из информации о его структуре, хранящейся
 * в pojoInfo Map-е.
 */
public class PojoTextBuilder {
    /**
     * Экземпляр информационного слепка со Schema с соответствующей ей структурой
     * ключи - ключевые слова JSchema,
     * значения - строки, либо List<Field>
     */
    private final Map<String, Object> pojoInfo;

    public PojoTextBuilder(Map<String, Object> pojoInfo) {
        this.pojoInfo = pojoInfo;
    }

    /**
     * Шаблон текста класса без содержимого тела класса
     * %1$s - текст комментария,
     * %2$s - текст адреса package,
     * %3$s - текст имени класса,
     * %4$s - текст блока кода класса.
     */
    private static final String CLASS_TEMPLATE =
            "%1$s\n" +
                    "package %2$s;\n\n" +
                    "import java.util.List;\n" +
                    "import java.util.Objects;\n\n" +
                    "\npublic class %3$s {\n%4$s \n}\n";
    /**
     * Шаблон текста комментария
     * %s - текст комментария
     */
    private static final String COMMENT_TEMPLATE = "%1$s/**\n %1$s* %2$s\n%1$s*/\n";
    /**
     * Шаблон текста поля класса
     * %1$s - модификатор final,
     * %2$s - тип (класс) поля,
     * %3$s - имя поля.
     */
    private static final String FIELD_TEMPLATE = "\t private %1$s %2$s %3$s;\n";
    /**
     * Шаблон конструктора класса
     * %1$s - имя класса,
     * %2$s - поля через запятую,
     * %3$s - блок кода конструктора.
     */
    private static final String CONSTRUCTOR_TEMPLATE = "\n\t public %1$s(%2$s) {\n%3$s\t}\n";
    /**
     * Шаблон выражения
     * %1$s - левая часть выражения,
     * %2$s - праввая часть выражения.
     */
    private static final String EXPRESSION_TEMPLATE = "\t\t %1$s = %2$s;\n";
    /**
     * Шаблон геттера для поля
     * %1$s - тип (класс) поля,
     * %2$s - имя поля с большой буквы,
     * %3$s - имя поля.
     */
    private static final String GETTER_TEMPLATE = "\n\tpublic %1$s get%2$s() {\n\t\treturn %3$s;\n\t}\n";
    /**
     * Шаблон метода equals() класса
     * %1$s - тип класса,
     * %2$s - имя поля - как имя класса, но с прописной буквы,
     * %3$s - текст выражения возвращаемого методом.
     */
    private static final String EQUALS_TEMPLATE = "\n\t@Override\n" +
            "    public boolean equals(Object o) {\n" +
            "        if (this == o) return true;\n" +
            "        if (!(o instanceof %1$s)) return false;\n" +
            "        %1$s %2$s = (%1$s) o;\n" +
            "        return %3$s;\n\t}\n";
    /**
     * Шаблон выражения после return метода equals()
     * %1$s - имя поля,
     * %2$s - имя класса с прописной буквы.
     */
    private static final String EQUALS_RETURN_TEMPLATE = "Objects.equals(%1$s, %2$s.%1$s)";
    /**
     * Шаблон выражения метода hashCode()
     * %s - текст параметров меода Objects.hash: имена полей класса через запятую.
     */
    private static final String HASH_TEMPLATE = "\n\t@Override\n" +
            "    public int hashCode() {\n" +
            "        return Objects.hash(%s);\n" +
            "    }\n";


    /**
     * Метод получения из объекта {@code pojoInfo} текста файла .java POJO-класса.
     *
     * @return текст POJO класса.
     */
    public String getClassContentText() {
        Object fieldsObject = pojoInfo.get(SCHEMA_PROPERTIES);
        if (Objects.nonNull(fieldsObject) &&
                fieldsObject instanceof List<?> &&
                ((List<?>) fieldsObject).get(0) instanceof Field) {
            List<Field> fields = (List<Field>) fieldsObject;
            return String.format(CLASS_TEMPLATE,
                    pojoInfo.get(SCHEMA_DESCRIPTION) != null ?
                            String.format(COMMENT_TEMPLATE, EMPTY_STRING, pojoInfo.get(SCHEMA_DESCRIPTION)) : EMPTY_STRING,
                    pojoInfo.get(PACKAGE),
                    pojoInfo.get(SCHEMA_TITLE),
                    classBody(fields));
        }
        throw new IllegalArgumentException("Is not a List<Fields>: "
                + fieldsObject.toString());
    }

    /**
     * Метод получения блока кода POJO-класса без обрамляющих фигурных скобок
     *
     * @param fields список Field - объектов полей класса.
     * @return строку блока кода без обрамляющих фигурных скобок
     */
    private String classBody(List<Field> fields) {
        return fieldsDeclarations(fields) +
                constructor(fields) +
                getters(fields) +
                equals_hashCode(fields);
    }

    /**
     * Метод получения текста полей POJO-класса.
     *
     * @param fields список Field - объектов полей класса.
     * @return текст полей класса.
     */
    private String fieldsDeclarations(List<Field> fields) {
        return fields.stream()
                .map(field ->
                        String.format(
                                Objects.isNull(field.getDescription()) ? "" :
                                        COMMENT_TEMPLATE, "\t" ,field.getDescription()) +
                        String.format(FIELD_TEMPLATE,
                                field.isFinal() ? "final" : EMPTY_STRING,
                                field.getType(),
                                field.getName()))
                .collect(Collectors.joining());
    }

    /**
     * Метод получения текста кода конструктора, инициализирующего все поля POJO-класса.
     *
     * @param fields список Field - объектов полей класса.
     * @return текст кода конструктора класса.
     */
    private String constructor(List<Field> fields) {
        String constructorParameters = fields.stream()
                .map(field -> field.getType() + " " + field.getName())
                .collect(Collectors.joining(", "));
        String constructorCode = fields.stream()
                .map(field -> String.format(EXPRESSION_TEMPLATE,
                        "this." + field.getName(),
                        field.getName()))
                .collect(Collectors.joining());

        return String.format(CONSTRUCTOR_TEMPLATE,
                pojoInfo.get(SCHEMA_TITLE),
                constructorParameters,
                constructorCode);
    }

    /**
     * Метод получения текста кода геттеров всех полей POJO-класса.
     *
     * @param fields список Field - объектов полей класса.
     * @return строку кода геттеров.
     */
    private String getters(List<Field> fields) {
        return fields.stream()
                .map(field -> String.format(GETTER_TEMPLATE,
                        field.getType(),
                        withCapitalLetter(field.getName()),
                        field.getName()))
                .collect(Collectors.joining());
    }

    /**
     * Метод получения текста кода equals и hashCode POJO-класса.
     *
     * @param fields список Field - объектов полей класса.
     * @return текст кода equals и hashCode.
     */
    private String equals_hashCode(List<Field> fields) {
        String equalsReturnString = fields.stream()
                .map(field -> String.format(EQUALS_RETURN_TEMPLATE,
                        field.getName(), field.getType()))
                .collect(Collectors.joining(" && "));

        String type = (String) pojoInfo.get(SCHEMA_TITLE);
        String equals = String.format(EQUALS_TEMPLATE,
                type,
                withLowercaseLetter(type).equals(type) ? "_" + type : withLowercaseLetter(type),
                equalsReturnString);

        String fieldNamesForHashCode = fields.stream()
                .map(Field::getName)
                .collect(Collectors.joining(", "));

        String hashCode = String.format(HASH_TEMPLATE, fieldNamesForHashCode);
        return equals + hashCode;
    }

}