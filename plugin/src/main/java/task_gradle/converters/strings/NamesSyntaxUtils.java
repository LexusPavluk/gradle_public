/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters.strings;

import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static task_gradle.Task_gradlePlugin.FILE_SUFFIX_REGEX;
import static task_gradle.converters.strings.StringUtils.*;

public class NamesSyntaxUtils {
    // patterns matched Java convention
    public static final String SENTENCE_TO_WORDS_DELIMITER_REGEX = "[\\p{P}&&[^_]]|\\s";
    private static final Pattern CLASS_NAME_PATTERN = Pattern.compile("[_$\\p{Upper}]+[\\p{Alnum}_$]*");
    private static final Pattern FIELD_NAME_PATTERN = Pattern.compile("[_$\\p{Lower}]+[\\p{Alnum}_$]*");
    public static final String FILENAME_EXTENSION = "\\.\\w*";
    public static final Pattern FILENAME_PATTERN = Pattern.compile("[^|<>\\\\:\"/?*]*" + FILENAME_EXTENSION);
    public static final String NEEDLESS_FILENAME_WORDS = "(" + FILE_SUFFIX_REGEX + ")|" + FILENAME_EXTENSION;
    private static final String INAPPROPRIATE_SIGNS = "[^\\p{Alnum}_$]+";
    private static final Pattern INVALID_CHARS_PATTERN = Pattern.compile(INAPPROPRIATE_SIGNS);
    // Java keywords by: https://docs.oracle.com/javase/tutorial/java/nutsandbolts/_keywords.html
    private static final String[] JAVA_KEYWORDS = {"abstract", "continue", "for", "new", "switch",
            "assert", "default", "goto", "package", "synchronized", "boolean", "do", "if",
            "private", "this", "break", "double", "implements", "protected", "throw",
            "byte", "else", "import", "public", "throws", "case", "enum", "instanceof",
            "return", "transient", "catch", "extends", "int", "short", "try", "char",
            "final", "interface", "static", "void", "class", "finally", "long", "strictfp",
            "volatile", "const", "float", "native", "super", "while", "true", "false", "null"};

    /**
     * @param name строка, представляющая собой возможное имя класса
     * @return строку - имя класса, сответствующее Java code conventions.
     */
    public static String className(String name) {
        if (!isClassName(name)) {
            String result = javaName(name);
            if (isLowerFirstChar(result))
                return className(javaName(firstSign(result).toUpperCase() + result.substring(1)));
            return result;
        }
        return name;
    }

    private static boolean isClassName(String name) {
        return CLASS_NAME_PATTERN.matcher(name).matches();
    }

    /**
     * @param name строка, представляющая собой возможное имя поля
     * @return строку - имя поля, сответствующее Java code conventions.
     */
    public static String fieldName(String name) {
        return isFieldName(name) ?
                name :
                fieldName(javaName(name));
    }

    private static boolean isFieldName(String name) {
        return FIELD_NAME_PATTERN.matcher(name).matches();
    }

    /**
     * @param name - преобразует строку к имени, соответствующему Java Code Conventions
     * @return строку соответствующую Java Code Conventions
     */
    private static String javaName(String name) {
        String result = stringToCamelCase(
                jsonFileNameWithoutExtAndSchema(name)
        );
        if (isInvalidCharsPresentInName(result))
            return javaName(result.replaceAll(INAPPROPRIATE_SIGNS, EMPTY_STRING));
        else if (isJavaKeyWord(result) || isNumberFirstChar(result))
            return javaName("_" + result);
        else return result;
    }

    private static boolean isJavaKeyWord(String word) {
        return Arrays.asList(JAVA_KEYWORDS).contains(word);
    }

    private static boolean isInvalidCharsPresentInName(String name) {
        return INVALID_CHARS_PATTERN.matcher(name).find();
    }

    /**
     * @param filename - строка - имя файла с расширением
     * @return имя файла без расширения и указания на Schema
     */
    static String jsonFileNameWithoutExtAndSchema(String filename) {
        return filename.replaceAll(NEEDLESS_FILENAME_WORDS, EMPTY_STRING);
    }

    /**
     * @param string - строка преобразуемая к имени класса
     * @return строку, такую что каждое слово исходной строки - с заглавной буквы и без пробелов
     */
    static String stringToCamelCase(String string) {
        return Arrays.stream(string.split(SENTENCE_TO_WORDS_DELIMITER_REGEX))
                        .map(StringUtils::withCapitalLetter)
                        .collect(Collectors.joining()
        );
    }

}
