/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */

package task_gradle.converters.strings;

public class StringUtils {
    public static final String EMPTY_STRING = "";

    private StringUtils() {
    }

    /**
     * @param str строка из которой берётся первый символ
     * @return первый символ строки str.
     */
    public static String firstSign(String str) {
        return str.length() > 0 ? str.substring(0, 1) : "";
    }

    /**
     * @param str строка, преобразуемая к строке с заглавной буквы
     * @return строку с заглавной буквы
     */
    public static String withCapitalLetter(String str) {
        return str.length() > 0 ?
                firstSign(str).toUpperCase() + str.substring(1) : "";
    }

    /**
     * @param str строка, преобразуемая к строке с заглавной буквы
     * @return строку с заглавной буквы
     */
    public static String withLowercaseLetter(String str) {
        return str.length() > 0 ?
                firstSign(str).toLowerCase() + str.substring(1) : "";
    }

    static boolean isLowerFirstChar(String name) {
        return firstSign(name).matches("[\\p{Lower}]");
    }

    static boolean isNumberFirstChar(String name) {
        return firstSign(name).matches("[\\p{Digit}]");
    }

}
