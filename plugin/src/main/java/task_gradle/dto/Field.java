/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */
package task_gradle.dto;

import java.util.Objects;

/**
 * DTO - класс, представляющий собой информацию о свойствах поля: тип, имя, финальноеЛи,
 * комментарий к нему.
 */
public class Field {
    final String type;
    final String name;
    private final boolean isFinal;
    private final String description;

    public Field(String type, String name, boolean isFinal, String description) {
        this.type = type;
        this.name = name;
        this.isFinal = isFinal;
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Field)) return false;
        Field field = (Field) o;
        return isFinal == field.isFinal && Objects.equals(description, field.description) &&
                Objects.equals(name, field.name) && Objects.equals(type, field.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, isFinal, name, type);
    }
}
