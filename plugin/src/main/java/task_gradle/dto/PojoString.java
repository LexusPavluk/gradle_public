/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */
package task_gradle.dto;

import java.util.Objects;

/**
 * DTO-класс для передачи информации об имени и содержимом будущего POJO.java файла.
 */
public class PojoString {

    /**
     * Имя класса (файла)
     */
    private final String pojoName;
    /**
     * Содержимое будущего POJO-файла.
     */
    private final String pojoString;

    public PojoString(String name, String pojoString) {
        this.pojoName = name;
        this.pojoString = pojoString;
    }

    public String getPojoName() {
        return pojoName;
    }

    public String getPojoString() {
        return pojoString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PojoString)) return false;
        PojoString pojo = (PojoString) o;
        return pojoString.equals(pojo.pojoString);
    }

    @Override
    public int hashCode() {
        return Objects.hash(1, pojoString);
    }

}
