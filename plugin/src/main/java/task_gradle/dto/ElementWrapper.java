/*
 * Copyright (c) 2021.
 * by Alexey Pavlyuchenkov
 * email: lexuspavluk@gmail.com
 */
package task_gradle.dto;

import java.util.Objects;

/**
 * DTO-класс внутренней передачи информации о части текста JsonSchema при парсинге и
 * преобразовании в PojoString.
 */
public class ElementWrapper {

    /**
     * Идентификатор части JsonSchema
     */
    protected final String name;
    /**
     * Содержимое, соответствующее идентификатору (String, Field, List<Field>...
     */
    protected final Object value;

    public ElementWrapper(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ElementWrapper)) return false;
        ElementWrapper that = (ElementWrapper) o;
        return Objects.equals(name, that.name) && Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value);
    }
}
