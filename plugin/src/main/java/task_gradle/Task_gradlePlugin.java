/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package task_gradle;

import org.gradle.api.Project;
import org.gradle.api.Plugin;
import org.gradle.api.Task;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.TaskAction;
import task_gradle.converters.*;
import task_gradle.dto.ElementWrapper;
import task_gradle.dto.PojoString;

import java.io.File;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

/**
 * A plugin converting json-Schemas to POJO classes.
 */
public class Task_gradlePlugin implements Plugin<Project> {
    @Input
    String schemasFolder = "src\\test\\resources\\files\\";
    @Input
    String pojoFolder = "src\\test\\resources\\pojos\\";
    @Input
    String EXTENSION = ".json";
    @Input
    public static String FILE_SUFFIX_REGEX = "[Ss]chema";


    public void apply(Project project) {
        // Register a task
        project.getTasks().register(
                "convertJsonSchemaToPojo",
                task -> task.doLast(this::convertToPojo));
    }

    @TaskAction
    private void convertToPojo(Task task) {
        Converter<String, Set<File>> pathToFiles =
                new DirPathToFilesConverter(EXTENSION, FILE_SUFFIX_REGEX);
        Converter<File, ElementWrapper> toJsonConverter = new FileToJsonElemConverter();
        Converter<ElementWrapper, PojoString> toPojoConverter =
                new SchemaToPojoConverter(pojoFolder);
        Converter<PojoString, File> toFileConverter = new PojoToFileConverter(pojoFolder);

        long converted = pathToFiles.convert(schemasFolder).stream()
                .flatMap(Collection::stream)
                .map(toJsonConverter::convert)
                .map(Optional::orElseThrow)
                .map(toPojoConverter::convert)
                .map(Optional::orElseThrow)
                .map(toFileConverter::convert)
                .map(Optional::orElseThrow)
                .count();
        System.out.println("Converted--- to Pojo files from json-schemas " +
                converted + " files");
    }

}
